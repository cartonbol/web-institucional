@extends('layouts.layout')
@section('title','Monitoreo de Servicio')
@section('content')

    <monitor></monitor>

    <form>
        <input type="hidden" value="{{route('car.listInService')}}" ref="listCarRoute">
        <input type="hidden" value="{{route('client.listEnabled')}}" ref="listClientRoute">
        <input type="hidden" value="{{route('person.store')}}" ref="storeClientRoute">
        <input type="hidden" value="{{route('registerCall.assigned')}}" ref="registerLisRoute">
        <input type="hidden" value="{{route('registerCall.endService')}}" ref="endServiceRoute">
        <input type="hidden" value="{{route('registerCall.save')}}" ref="saveRegisterRoute">

    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/monitor.js')}}"></script>
@endsection
