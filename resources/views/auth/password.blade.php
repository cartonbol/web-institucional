@extends('layouts.layout')
@section('title','Cambiar password')
@section('content')
<div class="row">
    <div class="col-md-12">
            <div class="card">
                    <div class="card-header card-header-icon" data-background-color="rose">
                        <i class="material-icons">settings_phone</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Cambiar password del usuario</h4>
                        <div class="row">
                            <div class="col-md-6">
                                
                                <form action="{{route('user.change_password')}}" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('PUT')}}
                                    <div class="form-group label-floating is-empty" ref="clientName">
                                        <label class="control-label">Contraseña actual</label>
                                        <input autocomplete="off" type="password" 
                                        class="form-control" name="old_password">
                                        <span class="material-input"></span>
                                    </div>
                                    <div class="form-group label-floating is-empty" ref="clientName">
                                        <label class="control-label">Nuevo contraseña</label>
                                        <input autocomplete="off" type="password" 
                                        class="form-control" name="password1" >
                                        <span class="material-input"></span>
                                    </div>
                                    <div class="form-group label-floating is-empty" ref="clientName">
                                        <label class="control-label">Repita contraseña</label>
                                        <input autocomplete="off" type="password" 
                                        class="form-control" name="password2">
                                        <span class="material-input"></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Guardar" class="btn btn-primary">
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if (session('message'))
                                        <div class="alert alert-danger">
                                            {{ session('message') }}
                                        </div>
                                    @endif
                                </form>
                            </div>
                            <div class="col-md-6">
                                
                            </div>
                        </div>
                    </div>

                </div>
    </div>  
</div>        
    
@endsection

@section('scripts')
    
@endsection
