@extends('layouts.layout')
@section('title','Registrar Servicio')
@section('content')

    <registers></registers>

    <form>
        <input type="hidden" value="{{route('car.listInService')}}" ref="listCarRoute">
        <input type="hidden" value="{{route('client.listEnabled')}}" ref="listClientRoute">
        <input type="hidden" value="{{route('person.store')}}" ref="storeClientRoute">
        <input type="hidden" value="{{route('registerCall.listRequestedCalls')}}" ref="registerLisRoute">
        <input type="hidden" value="{{route('registerCall.assignCar')}}" ref="assignCarRoute">
        <input type="hidden" value="{{route('registerCall.save')}}" ref="saveRegisterRoute">
        <input type="hidden" value="{{route('registerCall.index')}}" ref="saveDestinationRoute">

    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/register.js')}}"></script>
@endsection
