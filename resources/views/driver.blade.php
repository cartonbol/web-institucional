@extends('layouts.layout')
@section('title','Empleados')
@section('content')

    <employee></employee>

    <form>
        <input type="hidden" value="{{route('person.list')}}" ref="listEmployeeRoute">
        <input type="hidden" value="{{route('person.store')}}" ref="storeEmployeeRoute">
    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/employee.js')}}"></script>
@endsection
