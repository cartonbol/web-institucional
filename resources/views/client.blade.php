@extends('layouts.layout')
@section('title','Clientes')
@section('content')

    <clients></clients>

    <form>
        <input type="hidden" value="{{route('person.list')}}" ref="listClientRoute">
        <input type="hidden" value="{{route('person.store')}}" ref="storeClientRoute">
    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/client.js')}}"></script>
@endsection
