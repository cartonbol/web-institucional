@extends('layouts.layout')
@section('title','Vehículos')
@section('content')

    <cars></cars>

    <form>
        <input type="hidden" value="{{route('car.list')}}" ref="listCarRoute">
        <input type="hidden" value="{{route('car.store')}}" ref="storeCarRoute">
        <input type="hidden" value="{{route('person.driverEnabledList')}}" ref="listDriverRoute">
        <input type="hidden" value="{{route('person.list')}}" ref="getDriverRoute">

    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/car.js')}}"></script>
@endsection
