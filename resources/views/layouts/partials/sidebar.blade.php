<div class="sidebar" data-active-color="rose" data-background-color="black" data-image="{{asset('img/sidebar-1.jpg')}}">
    <div class="logo">
        <a href="#" class="simple-text logo-mini">
            PF
        </a>
        <a href="#" class="simple-text logo-normal">
            Panificadora
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{asset('img/faces/avatar.jpg')}}" />

            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseSesion" class="collapsed">
                    <span>
                        <span>{{Auth::check()?Auth::user()->username:'0'}}</span>
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="clearfix"></div>
                <div class="collapse" id="collapseSesion">
                    <ul class="nav">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                                <span class="sidebar-mini"> EP </span>
                                <span class="sidebar-normal"> Cerrar Sesión </span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        <li>
                            <a href="{{ route('user.reset_password') }}">
                                <span class="sidebar-mini"> CP </span>
                                <span class="sidebar-normal"> Cambiar password </span>
                            </a>
                        </li>

                        
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <li class="nav-item ">
                <a class="nav-link collapsed" data-toggle="collapse" href="#mapsAlmacen" aria-expanded="false">
                    <i class="material-icons">inventory_2</i>
                    <p> Almacen
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="mapsAlmacen">
                    <ul class="nav">
                        <li class="nav-ite">
                            <a class="nav-link" href="../examples/maps/google.html">
                                <span class="sidebar-mini"> Mov </span>
                                <span class="sidebar-normal"> Movimientos </span>
                            </a>
                        </li>
                        <li class="nav-ite">
                            <a class="nav-link" href="../examples/maps/fullscreen.html">
                                <span class="sidebar-mini"> Rep </span>
                                <span class="sidebar-normal"> Reportes</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link collapsed" data-toggle="collapse" href="#mapsProduccion" aria-expanded="false">
                    <i class="material-icons">build</i>
                    <p> Produccion
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="mapsProduccion" style="">
                    <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link" href="../examples/maps/google.html">
                                <span class="sidebar-mini"> Reg </span>
                                <span class="sidebar-normal"> Registros </span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="../examples/maps/fullscreen.html">
                                <span class="sidebar-mini"> Rep </span>
                                <span class="sidebar-normal"> Reportes</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link collapsed" data-toggle="collapse" href="#mapsVentas" aria-expanded="false">
                    <i class="material-icons">shopping_cart</i>
                    <p> Ventas
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="mapsVentas" style="">
                    <ul class="nav">
                        <li class="{{request()->is('*/conductores/*')?'active':''}}">
                            <a href="{{route('driver.index')}}">
                                <i class="material-icons">rowing</i>
                                <p> Clientes </p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="../examples/maps/google.html">
                                <span class="sidebar-mini"> Reg </span>
                                <span class="sidebar-normal"> Registros </span>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="../examples/maps/fullscreen.html">
                                <span class="sidebar-mini"> Rep </span>
                                <span class="sidebar-normal"> Reportes</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link collapsed" data-toggle="collapse" href="#mapsDespachos" aria-expanded="false">
                    <i class="material-icons">grid_on</i>
                    <p> Despachos
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="mapsDespachos" style="">
                    <ul class="nav">
                        <li class="{{request()->is('*/conductores/*')?'active':''}}">
                            <a href="{{route('driver.index')}}">
                                <i class="material-icons">rowing</i>
                                <p> Despachos </p>
                            </a>
                        </li>
                        <li class="{{request()->is('vehiculos')?'active':''}}">
                            <a href="{{route('car.index')}}">
                                <i class="material-icons">directions_car</i>
                                <p> Vehículos </p>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="../examples/maps/fullscreen.html">
                                <span class="sidebar-mini"> Rep </span>
                                <span class="sidebar-normal"> Reportes</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link collapsed" data-toggle="collapse" href="#mapsConfig" aria-expanded="false">
                    <i class="material-icons">settings</i>
                    <p> Configuracion
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="mapsConfig">
                    <ul class="nav">
                        <li class="{{request()->is('*/employee/*')?'active':''}}">
                            <a href="{{route('driver.index')}}">
                                <i class="material-icons">people</i>
                                <p> Empleados </p>
                            </a>
                        </li>
                        <li class="{{request()->is('*/user/*')?'active':''}}">
                            <a href="{{route('user.index')}}">
                                <i class="material-icons">rowing</i>
                                <p> Usuarios </p>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            {{--<li class="{{request()->is('registros')?'active':''}}">--}}
                {{--<a href="{{route('registerCall.index')}}">--}}
                    {{--<i class="material-icons">edit</i>--}}
                    {{--<p> Registro </p>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="{{request()->is('*/monitoreo')?'active':''}}">--}}
                {{--<a href="{{route('registerCall.showAssigned')}}">--}}
                    {{--<i class="material-icons">departure_board</i>--}}
                    {{--<p> Monitoreo </p>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{----}}
            {{--<li class="{{request()->is('tarifas')?'active':''}}">--}}
                {{--<a href="{{route('rate.index')}}">--}}
                    {{--<i class="material-icons">directions_car</i>--}}
                    {{--<p> Tarifas </p>--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<li class="{{request()->is('*/clientes/*')?'active':''}}">--}}
                {{--<a href="{{route('client.index')}}">--}}
                    {{--<i class="material-icons">today</i>--}}
                    {{--<p> Agenda </p>--}}
                {{--</a>--}}
            {{--</li>--}}


            {{--<li class="{{request()->is('*/historial')?'active':''}}">--}}
                {{--<a href="{{route('registerCall.showListAllCall')}}">--}}
                    {{--<i class="material-icons">directions_car</i>--}}
                    {{--<p> Usuarios </p>--}}
                {{--</a>--}}
            {{--</li>--}}

           

        </ul>
    </div>
</div>
