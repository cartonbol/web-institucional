@extends('layouts.layout')
@section('title','Tarifas')
@section('content')

    <rates></rates>

    <form>
        <input type="hidden" value="{{route('rate.list')}}" ref="listRateRoute">
        <input type="hidden" value="{{route('rate.store')}}" ref="storeRateRoute">
        <input type="hidden" value="{{route('rate.index')}}" ref="indexRateRoute">

    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/rate.js')}}"></script>
@endsection
