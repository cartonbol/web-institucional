@extends('layouts.layout')
@section('title','Usuarios')
@section('content')

    <drivers></drivers>

    <form>
        <input type="hidden" value="{{route('user.list')}}" ref="listUserRoute">
        <input type="hidden" value="{{route('user.store')}}" ref="storeUserRoute">
    </form>
@endsection

@section('scripts')
    <script src="{{asset('js/user.js')}}"></script>
@endsection
