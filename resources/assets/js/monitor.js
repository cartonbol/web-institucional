
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
//import Vuesax from 'vuesax'

//import 'vuesax/dist/vuesax.css' //vuesax styles
//window.Vue.use(Vuesax)
//window.alertify=require('alertify.js');
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('monitor', require('./components/MonitorComponent'));

const app = new Vue({
    el: '#app'
});
$("#modalForm").on('shown.bs.modal',function(e){
    $(this).find("input[name='firstname']").focus();
});
