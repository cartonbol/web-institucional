let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
  'node_modules/material-icons/css/material-icons.css',
  'resources/assets/project_assets/css/bootstrap.min.css',
  'resources/assets/project_assets/css/material-dashboard.css',
  'resources/assets/project_assets/css/demo.css'
],'public/css/layout.css')
.scripts([
  'resources/assets/project_assets/js/jquery-3.2.1.min.js',
  'resources/assets/project_assets/js/bootstrap.min.js',
  'resources/assets/project_assets/js/material.min.js',
  'resources/assets/project_assets/js/perfect-scrollbar.jquery.min.js',
  'resources/assets/project_assets/js/arrive.min.js',
  'resources/assets/project_assets/js/jquery.validate.min.js',
  'resources/assets/project_assets/js/moment.min.js',
  'resources/assets/project_assets/js/chartist.min.js',
  'resources/assets/project_assets/js/jquery.bootstrap-wizard.js',
  'resources/assets/project_assets/js/bootstrap-notify.js',
  'resources/assets/project_assets/js/jquery.sharrre.js',
  'resources/assets/project_assets/js/bootstrap-datetimepicker.js',
  'resources/assets/project_assets/js/jquery-jvectormap.js',
  'resources/assets/project_assets/js/nouislider.min.js',
  'resources/assets/project_assets/js/jquery.select-bootstrap.js',
  'resources/assets/project_assets/js/jquery.datatables.js',
  'resources/assets/project_assets/js/sweetalert2.js',
  'resources/assets/project_assets/js/jasny-bootstrap.min.js',
  'resources/assets/project_assets/js/fullcalendar.min.js',
  'resources/assets/project_assets/js/jquery.tagsinput.js',
  'resources/assets/project_assets/js/material-dashboard.js',
  'resources/assets/project_assets/js/demo.js'
],'public/js/layout.js');

mix.js('resources/assets/js/client.js','public/js/client.js');
mix.js('resources/assets/js/car.js','public/js/car.js');
mix.js('resources/assets/js/register.js','public/js/register.js');
mix.js('resources/assets/js/monitor.js','public/js/monitor.js');
mix.js('resources/assets/js/historic.js','public/js/historic.js');
mix.js('resources/assets/js/rate.js','public/js/rate.js');
//----------------------------
mix.js('resources/assets/js/configuracion/user.js','public/js/user.js');
mix.js('resources/assets/js/configuracion/employee.js','public/js/employee.js');


