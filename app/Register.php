<?php

namespace PanificadoraFreed;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $table = 'requisitions';

    protected $fillable = [
        'destination_address',
        'comment',
        'state',
        'service_status',
        'person_id',
        'car_id'
    ];

    public function people(){
        return $this->belongsTo(Person::class,'person_id','id');
    }

    public function car(){
        return $this->belongsTo(Car::class,'car_id','id');
    }
}
