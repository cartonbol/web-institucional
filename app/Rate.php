<?php

namespace PanificadoraFreed;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable=[
        'comments',
        'price',
        'destination'
    ];
}
