<?php

namespace PanificadoraFreed;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{

    protected $fillable=[
        'comments',
        'name',
        'address',
        'phone',
        'state',
        'type',
        'card_number'
    ];

    protected $casts=[
        'state'=>'boolean'
    ];

    public function user(){

        return $this->hasOne(User::class);

    }

    //When person is driver, he has driven cars history
    public function driven_cars(){

        return $this->belongsToMany(Car::class,'cars_drivers','person_id','car_id')
            ->withPivot(['state','created_at','updated_at'])
            ->withTimestamps();

    }

    //When person is client, he has requisitions
    public function calls(){

        return $this->belongsToMany(Car::class,'requisitions','person_id','car_id')
            ->withPivot(['destination_address','comment','state','service_status','created_at','updated_at'])
            ->withTimestamps();

    }

    //The person can be a owner car
    public function own_cars(){

        return $this->hasMany(Car::class,'person_id','id');

    }

    //The person drives a car actually
    public function car_drive(){

        return $this->hasMany(Car::class,'driver_id','id');

    }

    public function requisitions(){
        return $this->hasMany(Register::class,'person_id','id');
    }
}
