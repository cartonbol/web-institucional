<?php

namespace PanificadoraFreed;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $fillable=[
        'plate',
        'issued',
        'description',
        'state',
        'in_service',
        'person_id',
        'driver_id'
    ];

    //A car can be driven by many drivers, in different days
    public function drivers(){

        return $this->belongsToMany(Person::class,'cars_drivers','car_id','person_id')
            ->withPivot(['state','created_at','updated_at'])
            ->withTimestamps();

    }

    //A car can serve to many clients
    public function clients_served(){

        return $this->belongsToMany(Person::class,'requisitions','car_id','person_id')
                ->withPivot(['destination_address','comment','state','created_at','upadted_at'])
                ->withTimestamps();

    }


    //A car belongs to a owner
    public function owner(){

        return $this->belongsTo(Person::class,'person_id','id');

    }

    //A car is driven by a driver
    public function driver(){

        return $this->belongsTo(Person::class,'driver_id','id');

    }

    public function requisitions(){
        return $this->hasMany(Register::class,'car_id','id');
    }
}
