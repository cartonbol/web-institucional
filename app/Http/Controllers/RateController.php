<?php

namespace PanificadoraFreed\Http\Controllers;

use PanificadoraFreed\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rate');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(Request $request)
    {
        $perPage = $request->perPage;
        $searchText = $request->searchText;

        $rates = Rate::query()
                ->when($searchText,function($query) use ($searchText) {
                     $query->orWhere('comments','like','%'.$searchText.'%');
                     $query->orWhere('destination','like','%'.$searchText.'%');
                })
                ->orderBy('id','DESC')
                ->paginate($perPage);

        return response()->json($rates,200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'comments'=>'nullable|min:5',
            'price'=>'required|min:1',
            'destination'=>'required|min:5',
        ];
        $messages=[
            'comments.min'=>'Los detalles deben tener al menos 5 caracteres',
            'price.required'=>'El precio es requerido',
            'price.min'=>'El precio debe tener al menos un caracter',
            'destination.required'=>'El destino es requerido',
            'destination.min'=>'El destino debe tener al menos 5 caracteres',
        ];
        $this->validate($request,$rules,$messages);

        (new Rate())->create($request->all());

        return response()->json('Guardado exitosamente!',201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \PanificadoraFreed\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function show(Rate $rate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PanificadoraFreed\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function edit(Rate $rate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PanificadoraFreed\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rate $rate)
    {
        $rules=[
            'comments'=>'nullable|min:5',
            'price'=>'required|min:1',
            'destination'=>'required|min:5',
        ];
        $messages=[
            'comments.min'=>'Los detalles deben tener al menos 5 caracteres',
            'price.required'=>'El precio es requerido',
            'price.min'=>'El precio debe tener al menos un caracter',
            'destination.required'=>'El destino es requerido',
            'destination.min'=>'El destino debe tener al menos 5 caracteres',
        ];
        
        $this->validate($request,$rules,$messages);

        $rate->update($request->all());

        return response()->json('Guardado exitosamente!',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PanificadoraFreed\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rate $rate)
    {
        $rate->delete();

        return response()->json('correcto',200);
    }
}
