<?php

namespace PanificadoraFreed\Http\Controllers;

use Illuminate\Support\Facades\DB;
use PanificadoraFreed\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('car');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(Request $request)
    {
        $perPage = $request->perPage;
        $searchText = $request->searchText;

        $cars = Car::query()
               ->whereHas('driver',function ($query) use ($searchText) {
                   $query->when($searchText,function ($query) use ($searchText) {
                        $query->whereRaw("name like '%".$searchText."%'");
                    });
                })
                ->orWhereHas('owner',function ($query) use ($searchText) {
                    $query->when($searchText,function ($query) use ($searchText) {
                        $query->whereRaw("name like '%".$searchText."%'");
                    });
                })
                ->when($searchText,function($query) use ($searchText) {
                     $query->orWhere('plate','like','%'.$searchText.'%');
                     $query->orWhere('description','like','%'.$searchText.'%');
                })
                ->with(['driver','owner'])
                ->orderBy('id','DESC')
                ->paginate($perPage);

        return response()->json($cars,200);
    }

    public function listInService(Request $request){
        $searchText=$request->searchText;
        $cars = Car::query()
                ->when($searchText,function($query) use ($searchText) {
                    return $query->where('description','like',"%".$searchText."%");//numero de movil
                })
                ->whereRaw("in_service=true")
                ->with(['driver','owner'])
                ->orderBy('id','DESC')
                ->get();

        return response()->json($cars,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'description'=>'required|min:1',
            'driver_id'=>'required|numeric',
            'issued'=>'required',
            'person_id'=>'required|numeric',
            'plate'=>'required'

        ];
        $messages=[
            'description.required'=>'El número de móvil es requerido',
            'description.min'=>'El número de móvil debe contener al menos un caracter',
            'driver_id.required'=>'Seleccione un conductor para el vehículo',
            'person_id.required'=>'Seleccione un dueño para el vehículo',
            'issued.required'=>'Ingrese lugar de expedición de la placa',
            'plate.required'=>'Ingrese un número de placa para el vehículo',

        ];
        $this->validate($request,$rules,$messages);

        (new Car())->create($request->all());

        return response()->json('Guardado exitosamente!',201);

    }

    /**
     * Display the specified resource.
     *
     * @param  \PanificadoraFreed\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PanificadoraFreed\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PanificadoraFreed\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $rules=[
            'description'=>'required|min:1',
            'driver_id'=>'required|numeric',
            'issued'=>'required',
            'person_id'=>'required|numeric',
            'plate'=>'required'

        ];
        $messages=[
            'description.required'=>'El número de móvil es requerido',
            'description.min'=>'El número de móvil debe contener al menos un caracter',
            'driver_id.required'=>'Seleccione un conductor para el vehículo',
            'person_id.required'=>'Seleccione un dueño para el vehículo',
            'issued.required'=>'Ingrese lugar de expedición de la placa',
            'plate.required'=>'Ingrese un número de placa para el vehículo',

        ];

        $this->validate($request,$rules,$messages);

        $car->update($request->all());

        return response()->json('Guardado exitosamente!',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PanificadoraFreed\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function delete(Car $car){
        $car->state=!$car->state;
        $car->save();
        return response()->json('Guardado exitosamente!',200);
    }

    public function changeService(Car $car){
        $car->in_service=!$car->in_service;
        $car->save();
        return response()->json('Guardado exitosamente!',200);
    }
}
