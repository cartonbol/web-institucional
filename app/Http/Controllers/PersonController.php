<?php

namespace PanificadoraFreed\Http\Controllers;

use PanificadoraFreed\Person;
use Illuminate\Http\Request;

class PersonController extends Controller
{

    public function show($driver){

        $driver = Person::query()->findOrFail($driver);

        return $driver;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function driversIndex()
    {
        return view('driver');
    }
    public function clientsIndex()
    {
        return view('client');
    }

    public function list(Request $request){

        //return $request->all();
        $name=$request->name;
        $type=$request->type;
        $perPage=$request->perPage;
        $state=$request->state;

        $drivers=Person::query()
            ->whereRaw("type = '".$type."'")
            ->when($state,function ($query) use ($state) {
                $query->whereRaw("state = ".$state);
            })
            ->when($name,function($query) use ($name) {
                $query->whereRaw("name like '%".$name."%'");
                $query->OrWhereRaw("phone like '%".$name."%'");

            })
            ->orderBy('id','DESC')
            ->paginate($perPage);

        return response()->json($drivers,200);

    }

    public function getEnabledClients(Request $request){

        $phone=$request->phone;

        $clients=Person::query()
            ->whereRaw("phone like '%".$phone."%'")
            ->whereRaw("type='CLIENT'")
            ->whereRaw('state=true')
            ->get();

        return response()->json($clients,200);
    }

    public function driverEnabledList(Request $request){
        $name = $request->name;

        $people = Person::query()
            ->whereRaw("type='DRIVER'")
            ->whereRaw("name like '%".$name."%'")
            ->whereRaw("state=true")
            ->get();

        return response()->json($people,200);
    }

    public function store(Request $request){

        $rules=[
            'name'=>'required',
            'comments'=>'nullable',
            'address'=>'required',
            'phone'=>'required',
            'type'=>'required',
        ];

        $this->validate($request,$rules);

        Person::query()->create($request->all());

        return response()->json('Guardado exitosamente!',201);

    }
    public function update(Request $request,Person $person){

        $rules=[
            'name'=>'required',
            'comments'=>'nullable',
            'address'=>'required',
            'phone'=>'required',
            'type'=>'required',
        ];

        $this->validate($request,$rules);

        $person->update($request->all());

        return response()->json('Guardado exitosamente!',200);

    }

    public function delete(Person $person){
        $person->state=!$person->state;
        $person->save();
        return response()->json('Guardado exitosamente!',200);
    }

}
