<?php

namespace PanificadoraFreed\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Login2Controller extends Controller
{
    public function index(){
        //return view()
    }

    public function showResetPasswordForm(){
        return view('auth.password');
    }

    public function changePassword(Request $request){
       
        $rules=[
            'old_password'=>'required',
            'password1'=>'required|min:7',
            'password2'=>'same:password1'
        ];

        $messages=[
            'old_password.required'=>'La contraseña actual es requerida',
            'password1.required'=>'El nuevo password es requerido',
            'password1.min'=>'El nuevo password debe tener al menos 7 caracteres',
            'password2.same'=>'Las nuevas contraseñas no coinciden',
        ];
        
        $this->validate($request,$rules,$messages);

        $user=auth()->user();

        //return [$user->password, bcrypt($request->old_password)];
        if(Hash::check($request->old_password, $user->password)){
            $user->password=bcrypt($request->password1);
            $user->save();
            $request->session()->flush();
            return redirect()->route('login');
        }else{
            return back()->with(['message'=>'Contraseña incorrecta!']);
        }
        
    }
}
