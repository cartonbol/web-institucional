<?php

namespace PanificadoraFreed\Http\Controllers;

use PanificadoraFreed\Person;
use PanificadoraFreed\Register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegisterCallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('register');
    }


    public function listRequestedCalls(Request $request){
        
        $perPage=$request->perPage;
        
        $searchText=$request->searchText;

        $registers = Register::query()
                    ->where(function($query) use ($searchText){
                        $query->whereHas('people',function ($query) use ($searchText) {
                            $query->when($searchText,function ($query) use ($searchText) {
                                $query->whereRaw("name like '%".$searchText."%'");
                                $query->orWhereRaw("phone like '%".$searchText."%'");
                            });
                        });
                        //$query->orWhereRaw("comment like '%".$searchText."%'");
                        
                    })
                    ->whereRaw("service_status='REQUESTED'")
                    ->with(['people'])
                    ->orderBy('id','DESC')
                    ->paginate($perPage);

        return response()->json($registers,200);
    }

    public function showListCallAssigned(){
        return view('monitor');
    }
    public function listCallAssigned(Request $request){

        $perPage=$request->perPage;
        
        $searchText=$request->searchText;

        $registers = Register::query()
                    ->whereHas('people',function ($query) use ($searchText) {
                        $query->when($searchText,function ($query) use ($searchText) {
                            $query->whereRaw("name like '%".$searchText."%'");
                            $query->orWhereRaw("phone like '%".$searchText."%'");
                        });
                    })
                    ->whereRaw("service_status='ASSIGNED'")
                    ->with(['people','car.driver'])
                    ->orderBy('id','DESC')
                    ->paginate($perPage);

        return response()->json($registers,200);
    }
    public function showListAllCall(){
        return view('historic');
    }
    public function listAllCall(Request $request){

        $perPage=$request->perPage;
        
        $searchText=$request->searchText;
        $fecha_ini=$request->date0;
        $fecha_fin=$request->date1;

        $registers = Register::query()
                    ->whereHas('people',function ($query) use ($searchText) {
                        $query->when($searchText,function ($query) use ($searchText) {
                            $query->whereRaw("name like '%".$searchText."%'");
                            $query->orWhereRaw("phone like '%".$searchText."%'");
                        });
                    })
                    ->whereRaw("service_status='ENDED'")
                    ->when($fecha_ini && $fecha_fin, function ($query) use ($fecha_fin, $fecha_ini) {
                        //return $query->whereRaw("created_at BETWEEN '".$fecha_ini."' AND DATE'".$fecha_fin."' + INTERVAL '1 day'");
                          return $query->whereRaw("created_at BETWEEN '".$fecha_ini."' AND DATE_ADD('".$fecha_fin."',interval 1 DAY)");
                    })
                    ->with(['people','car.driver'])
                    ->orderBy('id','DESC')
                    ->paginate($perPage);

        return response()->json($registers,200);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function assignCar(Request $request)
    {
        $register_id =$request->id;
        $car_id=$request->car_id;
        $register = Register::findOrFail($register_id);

        $register->car_id=$car_id;
        $register->service_status='ASSIGNED';
        $register->save();

        return response()->json('asignado correctamente!',200);

    }

    public function endService(Request $request)
    {
        $register_id =$request->id;
        $register = Register::findOrFail($register_id);

        $register->service_status='ENDED';
        $register->save();

        return response()->json('terminado correctamente!',200);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $rules=[
            'destination_address'=>'nullable|min:1',
            'comment'=>'nullable|min:5',
            'client'=>'required',
            'client.id'=>'nullable',
            'client.name'=>'required|min:1',
            'client.phone'=>'required|min:1',
            'client.address'=>'required|min:1',
            'client.comments'=>'nullable|min:1'
        ];
        $this->validate($request,$rules);


        if ($request->client['id'])
            (new Register)->create([
                'destination_address'=>$request->destination_address,
                'person_id'=>$request->client['id']
            ]);
        else
        {
            try{
                DB::transaction(function() use ($request) {
                    $client = (new Person)->create([
                        'name'=>$request->client['name'],
                        'address'=>$request->client['address'],
                        'phone'=>$request->client['phone'],
                        'comments'=>$request->client['comments'],
                    ]);
                    (new Register)->create([
                        'destination_address'=>$request->destination_address,
                        'person_id'=>$client->id
                    ]);
                });
            }catch (\Exception $e){
                throw $e;
            }


        }

        return response()->json('Creado correctamente',201);


    }

    public function assignDestination(Request $request,Register $register){
        $register->destination_address=$request->destination_address;
        $register->save();
    }
    
}
