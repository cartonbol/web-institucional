<?php

use Faker\Generator as Faker;

$factory->define(PanificadoraFreed\Person::class, function (Faker $faker) {
    return [
        'card_number'=>rand(111111,9999999),
        'issued'=>$faker->randomElement(['OR','PT','LP','TJ','SC','CB','BN','PA','CH']),
        'lastname'=>$faker->lastName,
        'firstname'=>$faker->lastName,
        'name'=>$faker->name,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'state'=>$faker->randomElement([true,false]),
        'type'=>$faker->randomElement(['CLIENT','DRIVER']),
    ];
});
