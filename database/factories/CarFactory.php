<?php

use Faker\Generator as Faker;

$factory->define(PanificadoraFreed\Car::class, function (Faker $faker) {
    return [
        'plate'=>str_random(7),
        'issued'=>$faker->randomElement(['OR','PT','LP','TJ','SC','CB','BN','PA','CH']),
        'color'=>$faker->colorName,
        'description'=>$faker->text(100),
        'state'=>$faker->randomElement([true,false]),
        'in_service'=>$faker->randomElement([true,false]),
        'person_id'=>strtoupper(rand(1,50)),
        'driver_id'=>strtoupper(rand(1,50)),
    ];
});
