<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Database extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates',function (Blueprint $table){
            $table->increments('id');
            //$table->integer('card_number',false,true)->default(00000)->nullable();
            //$table->enum('issued',['OR','PT','LP','TJ','SC','CB','BN','PA','CH'])->default('CB')->nullable();
            //$table->string('lastname');
            //$table->string('firstname');
            $table->string('destination');
            $table->string('price');
            $table->string('comments')->nullable();
            $table->boolean('state')->default(1);
            $table->timestamps();
        });

        Schema::create('people',function (Blueprint $table){
            $table->increments('id');
            $table->string('card_number',50)->nullable();
            //$table->enum('issued',['OR','PT','LP','TJ','SC','CB','BN','PA','CH'])->default('CB')->nullable();
            //$table->string('lastname');
            //$table->string('firstname');
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('comments')->nullable();
            $table->boolean('state')->default(1);
            $table->enum('type',['CLIENT','DRIVER','EMPLOYEE'])->default('CLIENT');
            $table->timestamps();
        });

        Schema::create('cars',function (Blueprint $table){
            $table->increments('id');
            $table->string('plate');
            $table->enum('issued',['OR','PT','LP','TJ','SC','CB','BN','PA','CH']);
            //$table->string('color');
            $table->string('description');
            $table->boolean('state')->default(1);
            $table->boolean('in_service')->default(0);
            $table->integer('person_id')->unsigned();
            $table->integer('driver_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('cars_drivers',function (Blueprint $table){
            $table->increments('id');
            $table->boolean('state')->default(1);
            $table->integer('person_id')->unsigned();
            $table->integer('car_id')->unsigned();
            $table->timestamps();
        });

        Schema::create('requisitions',function (Blueprint $table){
            $table->increments('id');
            $table->string('destination_address')->nullable();
            //$table->text('comment')->nullable();
            $table->boolean('state')->default(1);
            $table->enum('service_status',['REQUESTED','ASSIGNED','ENDED'])->default('REQUESTED');
            $table->integer('person_id')->unsigned();
            $table->integer('car_id')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('cars',function (Blueprint $table){
            $table->foreign('person_id')
                ->references('id')->on('people')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('driver_id')
                ->references('id')->on('people')->onDelete('restrict')->onUpdate('cascade');
        });

        Schema::table('cars_drivers',function (Blueprint $table){
            $table->foreign('person_id')
                ->references('id')->on('people')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('car_id')
                ->references('id')->on('cars')->onDelete('restrict')->onUpdate('cascade');
        });

        Schema::table('requisitions',function (Blueprint $table){
            $table->foreign('person_id')
                ->references('id')->on('people')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('car_id')
                ->references('id')->on('cars')->onDelete('restrict')->onUpdate('cascade');
        });

        Schema::table('users',function (Blueprint $table){
            $table->foreign('person_id')
                ->references('id')->on('people')->onDelete('restrict')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars',function (Blueprint $table){
            $table->dropForeign('cars_person_id_foreign');
        });

        Schema::table('cars_drivers',function (Blueprint $table){
            $table->dropForeign('cars_drivers_car_id_foreign');
            $table->dropForeign('cars_drivers_person_id_foreign');
        });

        Schema::table('requisitions',function (Blueprint $table){
            $table->dropForeign('requisitions_car_id_foreign');
            $table->dropForeign('requisitions_person_id_foreign');
        });

        Schema::table('users',function (Blueprint $table){
            $table->dropForeign('users_person_id_foreign');
        });
        Schema::dropIfExists('people');
        Schema::dropIfExists('cars');
        Schema::dropIfExists('cars_drivers');
        Schema::dropIfExists('requisitions');
    }
}
