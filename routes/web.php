<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('login')->middleware('guest');
Auth::routes();

Route::get('/home','HomeController@index')->name('home')->middleware(['auth']);

Route::prefix('personas')->middleware(['auth'])->group(function (){

    Route::get('/','PersonController@list')->name('person.list');
    Route::get('/employee','PersonController@driverEnabledList')->name('person.driverEnabledList');
    Route::get('/{employee}','PersonController@show')->name('person.show');
    Route::post('/','PersonController@store')->name('person.store');
    Route::put('/{person}','PersonController@update')->name('person.update');
    Route::delete('/{person}','PersonController@delete')->name('person.delete');

    Route::prefix('employee')->group(function(){
        Route::get('/lista','PersonController@driversIndex')->name('person.list');

    });
    Route::prefix('clientes')->group(function(){
        Route::get('/lista','PersonController@clientsIndex')->name('client.index');
        Route::get('/lista-habilitados','PersonController@getEnabledClients')->name('client.listEnabled');

    });

});

Route::prefix('vehiculos')->middleware(['auth'])->group(function(){
    Route::get('/','CarController@index')->name('car.index');
    Route::get('/lista','CarController@listAll')->name('car.list');
    Route::get('/lista-en-servicio','CarController@listInService')->name('car.listInService');
    Route::post('/','CarController@store')->name('car.store');
    Route::delete('/{car}','CarController@delete')->name('car.delete');
    Route::post('/{car}/cambiar-servicio','CarController@changeService')->name('car.changeService');
    Route::put('/{car}','CarController@update')->name('car.update');


});

Route::prefix('registros')->middleware(['auth'])->group(function(){
    Route::get('/','RegisterCallController@index')->name('registerCall.index');
    Route::get('/lista-llamadas-solicitadas','RegisterCallController@listRequestedCalls')->name('registerCall.listRequestedCalls');
    Route::post('/asignar-vehiculo','RegisterCallController@assignCar')->name('registerCall.assignCar');
    Route::post('/terminar-servicio','RegisterCallController@endService')->name('registerCall.endService');
    Route::post('/guardar-registro','RegisterCallController@save')->name('registerCall.save');
    Route::get('/monitoreo','RegisterCallController@showListCallAssigned')->name('registerCall.showAssigned');
    Route::get('/lista-pedidos','RegisterCallController@listCallAssigned')->name('registerCall.assigned');
    Route::get('/historial','RegisterCallController@showListAllCall')->name('registerCall.showListAllCall');
    Route::get('/lista-historial','RegisterCallController@listAllCall')->name('registerCall.listAllCall');
    Route::put('/{register}','RegisterCallController@assignDestination')->name('registerCall.assignDestination');    

});

Route::prefix('tarifas')->middleware(['auth'])->group(function(){
    Route::get('/','RateController@index')->name('rate.index');
    Route::get('/lista','RateController@listAll')->name('rate.list');
    Route::post('/','RateController@store')->name('rate.store');
    Route::delete('/{rate}','RateController@destroy')->name('rate.delete');
    Route::put('/{rate}','RateController@update')->name('rate.update');
});


Route::Resource('user','UserController');
Route::prefix('user')->name('user.')->middleware(['auth'])->group(function(){
    Route::get('/list',"UserController@list")->name('list');
});

Route::prefix('usuarios')->middleware(['auth'])->group(function(){
    Route::get('/cambiar-password','Login2Controller@showResetPasswordForm')->name('user.reset_password');
    Route::put('/cambiar-password','Login2Controller@changePassword')->name('user.change_password');

});